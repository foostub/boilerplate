﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Windsor;

namespace UI
{
	public class Global : System.Web.HttpApplication
	{
		public static WindsorContainer Container;

		protected void Application_Start(object sender, EventArgs e)
		{
			AreaRegistration.RegisterAllAreas();
			var routes = RouteTable.Routes;
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Boilerplate", action = "Index", id = UrlParameter.Optional }
			);

			routes.MapMvcAttributeRoutes();

			// bind container
			Container = new WindsorContainer();
			Container.AddFacility<PersistenceFacility>();

			//Container.Register(
			//	Component.For<ElasticSearchHelper>()
			//		.UsingFactoryMethod(kernel => new ElasticSearchHelper())
			//		.LifestyleSingleton()
			//);

			// register mvc controllers so ctor arguments can be resolved
			Container.Register(Classes.FromThisAssembly()
				.BasedOn<IController>()
				.LifestyleTransient());

			var controllerFactory = new WindsorControllerFactory(Container.Kernel);
			ControllerBuilder.Current.SetControllerFactory(controllerFactory);

		}
	}
}