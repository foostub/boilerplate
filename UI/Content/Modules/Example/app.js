﻿angular.module('Example', ['ngRoute', 'ExampleControllers'])
	.factory('dataService', ['$http', function ($http) {
		var service = {};
		service.search = function (start, provider, speed, dealType, contractLength, usage, monthlyCost) {
			if (dealType == null) {
				dealType = "";
			}

			if (provider == null) {
				provider = "";
			}

			return $http.get(BASE_URL + "boilerplate/search?start=" + start + "&provider=" + provider + "&speed=" + speed + "&dealType=" + dealType + "&contractLength=" + contractLength + "&contractLength=" + contractLength + "&usage=" + usage + "&=monthlyCost" + monthlyCost);
		};
		return service;
	}])
	.config(['$routeProvider',
		function ($routeProvider) {
			$routeProvider.
				when('/foo/:provider?/:speed?/:type?/:contract?/:usage?/:monthly?', {
					templateUrl: BASE_URL + 'content/modules/example/views/home.html',
					controller: 'FooController',
					reloadOnSearch: false
				}).
				otherwise({
					redirectTo: '/foo'
				});
		}]);

