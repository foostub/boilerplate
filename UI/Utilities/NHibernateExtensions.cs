﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;

namespace UI.Utilities
{
	public static class NHibernateExtensions
	{
		public static IFutureValue<TResult> ToFutureValue<TSource, TResult>(
			this IQueryable<TSource> source,
			Expression<Func<IQueryable<TSource>, TResult>> selector)
			where TResult : struct
		{
			
			var provider = (DefaultQueryProvider)source.Provider;
			var method = ((MethodCallExpression)selector.Body).Method;
			var expression = Expression.Call(null, method, source.Expression);
			return (IFutureValue<TResult>)provider.ExecuteFuture(expression);
		}

		public static IEnumerable<T> GetPage<T>(this IEnumerable<T> source, int pageIndex, int pageSize)
		{
			return source.Skip(pageIndex * pageSize).Take(pageSize);
		}

		public static PagedList<T> FetchPaged<T>(this IQueryable<T> query, int pageIndex, int pageSize)
		{
			var futureCount = query.ToFutureValue(x => x.Count());
			return new PagedList<T>(query.Skip(pageIndex * pageSize).Take(pageSize).ToFuture(), pageIndex, pageSize, x => futureCount.Value);
		}

		public static PagedList<TOut> FetchPaged<T, TOut>(this IQueryable<T> query, int pageIndex, int pageSize, Func<T, TOut> transform)
		{
			var futureCount = query.ToFutureValue(x => x.Count());
			return new PagedList<TOut>(query.Skip(pageIndex * pageSize).Take(pageSize).ToFuture().ToArray().Select(transform), pageIndex, pageSize, x => futureCount.Value);
			//return new PagedList<T>(query.Skip(pageIndex * pageSize).Take(pageSize).ToFuture(), pageIndex, pageSize, x => futureCount.Value);
		}
	}
}
