﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Models
{
	// NOTE: it's a matter of opinion on where to keep validation. i'm doing it here 
	// to keep things simple. other approaches are to externalize with a validation 
	// library - FluentValidation, etc
	public class Example
	{
		public virtual Guid Id { get; set; }
		[Required]
		public virtual string Column1 { get; set; }
		public virtual IList<ExampleChild> Children { get; set; }

		public class Map : FluentNHibernate.Mapping.ClassMap<Example>
		{
			public Map()
			{
				this.Id(m => m.Id).GeneratedBy.GuidComb();
				this.Map(m => m.Column1).Length(100).Index("COLUMN_1");
				this.HasMany(m => m.Children);
			}
		}
	}
}