﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
	public class ExampleChild
	{
		public virtual Guid Id { get; set; }
		public virtual string FooField { get; set; }
		public virtual Example Parent { get; set; }
		public class Map : FluentNHibernate.Mapping.ClassMap<ExampleChild>
		{
			public Map()
			{
				this.Id(m => m.Id).GeneratedBy.GuidComb();
				this.Map(m => m.FooField).Length(100);
				// NOTE: i typically have an for foreign keys
				this.References(m => m.Parent).Index("EXAMPLE_CHILD_PARENT");
			}
		}
	}
}