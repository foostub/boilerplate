﻿using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Event;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Windsor
{
    public class PersistenceFacility : AbstractFacility
    {
        public static ISessionFactory SessionFactory;

        protected override void Init()
        {
            var config = Fluently.Configure()
                .Database(
                    MsSqlConfiguration
                    .MsSql2008
                    .ConnectionString(ConfigurationManager.AppSettings["ConnectionString"]))
                    .Mappings(m =>
                    {
                        m.FluentMappings.AddFromAssembly(typeof(UI.Models.Example).Assembly);
                        m.HbmMappings.AddFromAssembly(typeof(UI.Models.Example).Assembly);
                    })
                    .ExposeConfiguration(cfg =>
                    {
                        cfg.Properties.Add("hbm2ddl.keywords", "auto-quote");
                        // HACK: should not be used in a production system
						new SchemaUpdate(cfg).Execute(false, true);
                    });

            SessionFactory = config.BuildSessionFactory();

            Kernel.Register(
                Component.For<ISessionFactory>()
                    .UsingFactoryMethod(_ => SessionFactory), 
                Component.For<ISession>()
                    .UsingFactoryMethod(k => k.Resolve<ISessionFactory>().OpenSession())
                    .LifestyleTransient()
            );            
        }
    }
}